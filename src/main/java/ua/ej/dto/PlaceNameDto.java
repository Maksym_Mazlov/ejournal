package ua.ej.dto;



public class PlaceNameDto {
    private int id;
    private String techaddress;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTechaddress() {
        return techaddress;
    }

    public void setTechaddress(String techaddress) {
        this.techaddress = techaddress;
    }
}
