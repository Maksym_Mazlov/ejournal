package ua.ej.dto;

public class PlaceDto {
    private int id;
    private String techAddress;
    private String description;
    private String lastRevisionDate;
    private String address;
    private String ip;
    private String placetype;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTechAddress() {
        return techAddress;
    }

    public void setTechAddress(String techAddress) {
        this.techAddress = techAddress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLastRevisionDate() {
        return lastRevisionDate;
    }

    public void setLastRevisionDate(String lastRevisionDate) {
        this.lastRevisionDate = lastRevisionDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPlacetype() {
        return placetype;
    }

    public void setPlacetype(String placetype) {
        this.placetype = placetype;
    }
}
