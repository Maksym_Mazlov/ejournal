package ua.ej.mapper;

import org.springframework.stereotype.Component;
import ua.ej.domain.Place;
import ua.ej.dto.PlaceDto;
import ua.ej.util.DateConstants;

import java.util.ArrayList;
import java.util.List;

@Component
public class PlaceMapper {
    public List<PlaceDto> toDto(List<Place> places) {
        List<PlaceDto> placeDtoList = new ArrayList<>();

        for (Place p : places) {
            PlaceDto dto = new PlaceDto();
            dto.setId(p.getId());
            dto.setTechAddress(p.getTechAddress());
            dto.setDescription(p.getDescription());
            dto.setAddress(p.getAddress());
            dto.setIp(p.getIp());
            dto.setPlacetype(p.getPlaceType());
            if (p.getLastRevisionDate() != null) {
                dto.setLastRevisionDate(DateConstants.FULL_FORMAT.format(p.getLastRevisionDate()));
            } else {
                dto.setLastRevisionDate("");
            }

            placeDtoList.add(dto);
        }

        return placeDtoList;
    }
}
