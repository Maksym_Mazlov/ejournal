package ua.ej.mapper;

import org.springframework.stereotype.Component;
import ua.ej.domain.Place;
import ua.ej.dto.PlaceNameDto;

import java.util.ArrayList;
import java.util.List;

@Component
public class PlaceNameMapper {

    public List<PlaceNameDto> placeNameList(List<Place> places) {
        List<PlaceNameDto> placeNameDtoList = new ArrayList<>();

        for (Place p : places) {
            PlaceNameDto placeNamesDto = new PlaceNameDto();
            placeNamesDto.setId(p.getId());
            placeNamesDto.setTechaddress(p.getTechAddress());

            placeNameDtoList.add(placeNamesDto);
        }

        return placeNameDtoList;
    }
}
