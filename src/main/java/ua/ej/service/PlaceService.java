package ua.ej.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ua.ej.domain.Place;

import java.util.List;

@Service
public class PlaceService {

    private static final String INSERT_PLACE = "INSERT INTO place(techAddress, address, ip, placeType,description) VALUES (?,?,?,?,?)";
    private static final String SELECT_PLACE = "select * from place";

    @Autowired
    private JdbcTemplate jdbc;

    public void registerNewPlace(String techAddress, String address, String ip, String placeType, String description) {
        jdbc.update(INSERT_PLACE, techAddress, address, ip, placeType, description);
    }

    public List<Place> getAllPlaces() {
        return jdbc.query(SELECT_PLACE, new BeanPropertyRowMapper<>(Place.class));
    }


}
