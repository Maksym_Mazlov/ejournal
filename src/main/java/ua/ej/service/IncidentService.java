package ua.ej.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ua.ej.dto.IncidentDto;

import java.util.List;

@Service
public class IncidentService {

    private static final String INSERT_INCIDENT = "INSERT INTO incident(placeId, eventId, dateIncident,userid) VALUES (?,?, NOW(),?)";
    private static final String SELECT_INCIDENT = "select" +
            " i.id," +
            " i.eventId," +
            " i.placeId," +
            " i.dateIncident," +
            " u.login" +
            " from" +
            " incident as i" +
            " join" +
            " users as u" +
            " on i.userid = u.id";


    @Autowired
    private JdbcTemplate jdbc;

    public void registerIncident(String placeId, String eventId, int userId) {
        jdbc.update(INSERT_INCIDENT, placeId, eventId, userId);
    }

    public List<IncidentDto> getAllIncidents() {
        return jdbc.query(SELECT_INCIDENT, new BeanPropertyRowMapper<>(IncidentDto.class));
    }
}
