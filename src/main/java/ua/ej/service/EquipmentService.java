package ua.ej.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ua.ej.dto.EquipmentDto;

import java.util.List;

@Service
public class EquipmentService {
    private static final String INSERT_EQUIPMENT = "INSERT INTO equipment(systemBlock, monitorcashier, monitorpassenger,printer,ups,splitter,keyboard,placeId) VALUES (?,?,?,?,?,?,?,?)";

    private static final String SELECT_EQUIPMENT = "select" +
            " e.id," +
            " e.systemblock," +
            " e.monitorcashier," +
            " e.monitorpassenger," +
            " e.printer," +
            " e.ups," +
            " e.splitter," +
            " e.keyboard," +
            " p.techaddress" +
            " from" +
            " equipment as e" +
            " join place as p on" +
            " e.placeid = p.id";

    private static final String SELECT_EQUIPMENT_BY_TECHADDRESS = "select" +
            " e.id," +
            " e.systemblock," +
            " e.monitorcashier," +
            " e.monitorpassenger," +
            " e.printer," +
            " e.ups," +
            " e.splitter," +
            " e.keyboard," +
            " p.techaddress" +
            " from" +
            " equipment as e" +
            " join place as p on" +
            " e.placeid = p.id " +
            " where p.techaddress = ?";

    @Autowired
    private JdbcTemplate jdbc;


    public void AddEquipment(String systemBlock,
                             String monitorcashier,
                             String monitorpassenger,
                             String printer,
                             String ups,
                             String splitter,
                             String keyboard,
                             int placeId) {

        jdbc.update(INSERT_EQUIPMENT, systemBlock, monitorcashier, monitorpassenger, printer, ups, splitter, keyboard,placeId);
    }

    public List<EquipmentDto> getAllEquipment() {
        return jdbc.query(SELECT_EQUIPMENT, new BeanPropertyRowMapper<>(EquipmentDto.class));
    }

    public List<EquipmentDto> getAllEquipmentByTechAddress(String techaddress) {
        return jdbc.query(SELECT_EQUIPMENT_BY_TECHADDRESS, new BeanPropertyRowMapper<>(EquipmentDto.class),techaddress);
    }

}
