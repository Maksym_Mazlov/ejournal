package ua.ej.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ua.ej.domain.User;
import ua.ej.exception.EJException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@Service
public class UserService {

    private static final String COUNT_BY_LOGIN = "SELECT COUNT(*) FROM users where login = ?";
    private static final String INSERT_USER = "INSERT INTO users(login, password) VALUES (?,?)";
    private static final String SELECT_BY_EMAIL = "SELECT id FROM users WHERE login = ? AND password = ?";
    private static final String UPDATE_SESSION = "UPDATE users SET session = ? WHERE id = ?";
    private static final String COUNT_BY_SESSION = "select count(*) from users where session = ?";
    private static final String SELECT_USER_BY_SESSION = "select * from users where session = ?";

    @Autowired
    private JdbcTemplate jdbc;

    @Autowired
    private HttpServletRequest request;

    public void register(String login, String password) {
        // 1
        int count = jdbc.queryForObject(COUNT_BY_LOGIN, Integer.class, login);
        if (count > 0) {
            throw new EJException("Пользователь уже существует");
        }

        // 2
        jdbc.update(INSERT_USER, login, password);
    }

    public String authorize(String login, String password) {
        try {
            int id = jdbc.queryForObject(SELECT_BY_EMAIL, Integer.class, login, password);
            String session = UUID.randomUUID().toString();
            jdbc.update(UPDATE_SESSION, session, id);
            return session;
        } catch (EmptyResultDataAccessException e) {
            throw new EJException("Пользователя не существует или не правильный пароль");
        }
    }


    public boolean isUserSessionActive() {
        String session = getSessionFromCookies();
        if (session == null || session.isEmpty()) {
            return false;
        }

        return jdbc.queryForObject(COUNT_BY_SESSION, Integer.class, session) > 0;
    }

    public User currentUser() {
        String session = getSessionFromCookies();
        if (session == null || session.isEmpty()) {
            throw new EJException("Нету доступа");
        }

        try {
            return jdbc.queryForObject(SELECT_USER_BY_SESSION,
                    new BeanPropertyRowMapper<>(User.class),
                    session);
        } catch (EmptyResultDataAccessException e) {
            throw new EJException("Нету доступа");
        }
    }

    private String getSessionFromCookies() {
        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            return null;
        }

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("session")) {
                return cookie.getValue();
            }
        }
        return null;
    }

    public void removeUserSession() {
        String session = getSessionFromCookies();
        int id = currentUser().getId();

        if (session != null || !session.isEmpty()) {
            jdbc.update(UPDATE_SESSION, null, id);
        }
    }
}

