package ua.ej.exception;

public class EJException extends RuntimeException {
    public EJException(String message) {
        super(message);
    }
}
