package ua.ej.domain;

public class Equipment {

    private int id;
    private String systemBlock;
    private String monitorcashier;
    private String monitorpassenger;
    private String printer;
    private String ups;
    private String splitter;
    private String keyboard;
    private int placeId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSystemBlock() {
        return systemBlock;
    }

    public void setSystemBlock(String systemBlock) {
        this.systemBlock = systemBlock;
    }

    public String getMonitorcashier() {
        return monitorcashier;
    }

    public void setMonitorcashier(String monitorcashier) {
        this.monitorcashier = monitorcashier;
    }

    public String getMonitorpassenger() {
        return monitorpassenger;
    }

    public void setMonitorpassenger(String monitorpassenger) {
        this.monitorpassenger = monitorpassenger;
    }

    public String getPrinter() {
        return printer;
    }

    public void setPrinter(String printer) {
        this.printer = printer;
    }

    public String getUps() {
        return ups;
    }

    public void setUps(String ups) {
        this.ups = ups;
    }

    public String getSplitter() {
        return splitter;
    }

    public void setSplitter(String splitter) {
        this.splitter = splitter;
    }

    public String getKeyboard() {
        return keyboard;
    }

    public void setKeyboard(String keyboard) {
        this.keyboard = keyboard;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }
}
