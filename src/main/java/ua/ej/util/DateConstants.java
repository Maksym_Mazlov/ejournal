package ua.ej.util;

import java.time.format.DateTimeFormatter;

public class DateConstants {
    public static final DateTimeFormatter FULL_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private DateConstants() {
    }
}
