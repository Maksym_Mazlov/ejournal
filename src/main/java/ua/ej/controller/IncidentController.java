package ua.ej.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ua.ej.exception.EJException;
import ua.ej.service.IncidentService;
import ua.ej.service.UserService;

import java.util.HashMap;
import java.util.Map;

@Controller
public class IncidentController {

    @Autowired
    private IncidentService incidentService;

    @Autowired
    private UserService userService;

    @GetMapping(path = "/incident")
    public ModelAndView incidentPage() {
        if (!userService.isUserSessionActive()) {
            throw new EJException("Вы не авторизованы");
        }

        Map<String, Object> map = new HashMap<>();
        map.put("user", userService.currentUser());

        return new ModelAndView("incident", map);
    }

    @PostMapping(path = "/incident")
    public String addIncident(@RequestParam(name = "palaceId") String placeId,
                              @RequestParam(name = "eventId") String eventId) {
        if (!userService.isUserSessionActive()) {
            throw new EJException("Вы не авторизованы");
        }

        incidentService.registerIncident(placeId, eventId, userService.currentUser().getId());
        return "redirect:/main";
    }


}
