package ua.ej.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ua.ej.service.UserService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @GetMapping(path = "/login")
    public String loginPage() {
        return "login";
    }

    @PostMapping(path = "/login")
    public String loginUser(@RequestParam(name = "login") String login,
                            @RequestParam(name = "password") String password,
                            HttpServletResponse response) {
        String session = userService.authorize(login, password);
        response.addCookie(new Cookie("session", session));
        return "redirect:/main";
    }

    @GetMapping(value = "/logout")
    public String logoutUser(HttpServletResponse response) {
        Cookie cookie = new Cookie("session", "");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
        userService.removeUserSession();
        return "redirect:/login";
    }
}
