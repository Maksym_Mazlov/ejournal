package ua.ej.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.ej.domain.Place;
import ua.ej.domain.User;
import ua.ej.dto.EquipmentDto;
import ua.ej.dto.PlaceNameDto;
import ua.ej.exception.EJException;
import ua.ej.mapper.PlaceNameMapper;
import ua.ej.service.EquipmentService;
import ua.ej.service.PlaceService;
import ua.ej.service.UserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/equipment")
public class EquipmentController {

    @Autowired
    private UserService userService;

    @Autowired
    private PlaceService placeService;

    @Autowired
    private PlaceNameMapper placeNameMapper;

    @Autowired
    private EquipmentService equipmentService;


    @GetMapping(path = "/add")
    public ModelAndView showPageAddEquipment() {
        if (!userService.isUserSessionActive()) {
            throw new EJException("Вы не авторизованы");
        }

        User user = userService.currentUser();
        List<Place> placeList = placeService.getAllPlaces();
        List<PlaceNameDto> placeNameDtos = placeNameMapper.placeNameList(placeList);

        Map<String, Object> map = new HashMap<>();
        map.put("user", user);
        map.put("places", placeNameDtos);


        return new ModelAndView("addequipment", map);
    }


    @PostMapping(path = "/add")
    public String addNewEquipment(@RequestParam(name = "systemblock") String systemblock,
                                  @RequestParam(name = "monitorcashier") String monitorcashier,
                                  @RequestParam(name = "monitorpassenger") String monitorpassenger,
                                  @RequestParam(name = "printer") String printer,
                                  @RequestParam(name = "ups") String ups,
                                  @RequestParam(name = "splitter") String splitter,
                                  @RequestParam(name = "placeId") int placeId,
                                  @RequestParam(name = "keyboard") String keyboard) {

        if (!userService.isUserSessionActive()) {
            throw new EJException("Вы не авторизованы");
        }


        equipmentService.AddEquipment(systemblock, monitorcashier, monitorpassenger, printer, ups, splitter, keyboard,placeId );
        return "redirect:/addequipment";
    }


    @GetMapping(path = "/all")
    public ModelAndView showPageEquipment() {
        if (!userService.isUserSessionActive()) {
            throw new EJException("Вы не авторизованы");
        }

        User user = userService.currentUser();
        List<EquipmentDto> equipmentDtos = equipmentService.getAllEquipment();

        Map<String, Object> map = new HashMap<>();
        map.put("user", user);
        map.put("equipment", equipmentDtos);


        return new ModelAndView("equipment", map);
    }


    @GetMapping(path = "/place/{techaddress}")
    public ModelAndView showEquipmentInPlace(@PathVariable(name = "techaddress") String techaddress ) {
        if (!userService.isUserSessionActive()) {
            throw new EJException("Вы не авторизованы");
        }

        User user = userService.currentUser();
        List<EquipmentDto> equipmentDtos = equipmentService.getAllEquipmentByTechAddress(techaddress);

        Map<String, Object> map = new HashMap<>();
        map.put("user", user);
        map.put("equipment", equipmentDtos);


        return new ModelAndView("equipment", map);
    }


}
