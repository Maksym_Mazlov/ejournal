package ua.ej.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ua.ej.domain.User;
import ua.ej.dto.IncidentDto;
import ua.ej.exception.EJException;
import ua.ej.service.IncidentService;
import ua.ej.service.UserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class MainPageController {

    @Autowired
    private UserService userService;

    @Autowired
    private IncidentService incidentService;

    @GetMapping(value = {"/", "/main"})
    public ModelAndView showMainPage() {
        if (!userService.isUserSessionActive()) {
            throw new EJException("Вы не авторизованы");
        }

        User user = userService.currentUser();
        List<IncidentDto> incidents = incidentService.getAllIncidents();

        Map<String, Object> map = new HashMap<>();
        map.put("user", user);
        map.put("incidents", incidents);

        return new ModelAndView("main", map);
    }
}
