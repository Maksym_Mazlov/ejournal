package ua.ej.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ua.ej.domain.Place;
import ua.ej.domain.User;
import ua.ej.dto.PlaceDto;
import ua.ej.exception.EJException;
import ua.ej.mapper.PlaceMapper;
import ua.ej.service.PlaceService;
import ua.ej.service.UserService;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
public class PlaceController {
    @Autowired
    private UserService userService;

    @Autowired
    private PlaceService placeService;

    @Autowired
    private PlaceMapper placeMapper;

    @GetMapping(value = {"/place"})
    public ModelAndView showPlacePage() {
        if (!userService.isUserSessionActive()) {
            throw new EJException("Вы не авторизованы");
        }

        User user = userService.currentUser();
        List<Place> placeList = placeService.getAllPlaces();
        List<PlaceDto> placeDtoList = placeMapper.toDto(placeList);

        Map<String, Object> map = new HashMap<>();
        map.put("places", placeDtoList);
        map.put("user", user);
        map.put("timexxxx", LocalDateTime.now());

        return new ModelAndView("place", map);
    }


    @GetMapping(path = "/newPlace")
    public ModelAndView newPlacePage() {
        if (!userService.isUserSessionActive()) {
            throw new EJException("Вы не авторизованы");
        }
        User user = userService.currentUser();
        Map<String, Object> map = new HashMap<>();
        map.put("user", user);
        return new ModelAndView("newPlace", map);
    }

    @PostMapping(path = "/newPlace")
    public String addNewPlace(@RequestParam(name = "techAddress") String techAddress,
                              @RequestParam(name = "address") String address,
                              @RequestParam(name = "ip") String ip,
                              @RequestParam(name = "placeType") String placeType,
                              @RequestParam(name = "description") String description) {

        if (!userService.isUserSessionActive()) {
            throw new EJException("Вы не авторизованы");
        }

        placeService.registerNewPlace(techAddress, address, ip, placeType,description);
        return "redirect:/place";
    }

}
