package ua.ej.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ua.ej.service.UserService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Controller
public class RegisterController {

    @Autowired
    private UserService userService;

    @GetMapping(path = "/register")
    public String registerPage() {
        return "register";
    }

    @PostMapping(path = "/register")
    public String registerUser(@RequestParam(name = "login") String login,
                               @RequestParam(name = "password") String password,
                               HttpServletResponse response) {
        userService.register(login, password);
        String session = userService.authorize(login, password);
        response.addCookie(new Cookie("session", session));
        return "redirect:/main";
    }
}
