<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="/" class="site_title"><i class="fa fa-paw"></i> <span>Тех. журнал!</span></a>
        </div>
        <div class="clearfix"></div>
        <br/>
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3> Журнал </h3>
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-edit"></i> Обладнанння <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="/equipment/all">Все обладнання</a></li>
                            <li><a href="/equipment/all">Системний блок</a></li>
                            <li><a href="/equipment/all">Монітор касира</a></li>
                            <li><a href="/equipment/all">Монітор пасажира</a></li>
                            <li><a href="/equipment/all">Принтер</a></li>
                            <li><a href="/equipment/all">UPS</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-desktop"></i> Каси <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="/place">Всі каси</a></li>
                            <li><a href="/place">Експрес</a></li>
                            <li><a href="/place">Windows</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-table"></i> Програми <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="tables.html">Експрес</a></li>
                            <li><a href="tables_dynamic.html">Windows</a></li>
                            <li><a href="tables_dynamic.html">Lotus</a></li>
                            <li><a href="tables_dynamic.html">Personal communication</a></li>
                            <li><a href="tables_dynamic.html">Microsoft Word</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-bar-chart-o"></i> Працівники <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="chartjs.html">Відділ 1</a></li>
                            <li><a href="chartjs2.html">Відділ 2</a></li>
                            <li><a href="morisjs.html">Відділ 3</a></li>
                            <li><a href="echarts.html">Відділ 4</a></li>
                        </ul>
                    </li>

                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>