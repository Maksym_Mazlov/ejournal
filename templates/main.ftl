<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico"/>

    <title>Технічний журнал | </title>
    <#include 'blocks/meta-links.ftl'>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <#include 'blocks/sidebar.ftl'>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <img src="images/img.jpg" alt="">${user.login}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="/login">Login</a></li>
                                <li><a href="/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="images/img.jpg" alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <div class="text-center">
                                        <a>
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <a href="/incident"
                            <button type="button" class="btn btn-success"> + Додати несправність</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Черговий технічний журнал несправностей</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Дата</th>
                                <th>Каса</th>
                                <th>Несправність</th>
                                <th>Автор</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#list incidents as i>
                            <tr>
                                <td>${i.id}</td>
                                <td>${i.dateIncident}</td>
                                <td><a href="/equipment/place/${i.placeId}">${i.placeId}</a></td>
                                <td>${i.eventId}</td>
                                <td>${i.login}</td>
                            </tr>
                            </#list>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>


        </div>
        <!-- /page content -->

    <#include 'blocks/footer.ftl'>
    </div>
</div>

<!-- jQuery -->
<script src="/static/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="/static/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="/static/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="/static/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="/staticvendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="/static/vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="/static/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="/static/vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="/static/vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="/static/vendors/Flot/jquery.flot.js"></script>
<script src="/static/vendors/Flot/jquery.flot.pie.js"></script>
<script src="/static/vendors/Flot/jquery.flot.time.js"></script>
<script src="/static/vendors/Flot/jquery.flot.stack.js"></script>
<script src="/static/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="/static/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="/static/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="/static/vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="/static/vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="/static/vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="/static/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="/static/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="/static/vendors/moment/min/moment.min.js"></script>
<script src="/static/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="/static/build/js/custom.min.js"></script>

<!-- Datatables -->
<script src="/static/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/static/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/static/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="/static/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="/static/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="/static/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="/static/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="/static/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="/static/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="/static/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="/static/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="/static/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="/static/vendors/jszip/dist/jszip.min.js"></script>
<script src="/static/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="/static/vendors/pdfmake/build/vfs_fonts.js"></script>

</body>
</html>
